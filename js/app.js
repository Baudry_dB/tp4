
// faire $( function() { ... } ) sur toute la page pour évènement "ready" c'est à dire que le script s'exécute lorsque la page est prête

//alert('ici ce que tu veux');




$(function() {

  //Affichage des ingrédients au survol de leurs noms
  $("label").hover(function(){$(this).find('span.no-display').css("display","inline-block");},
	function(){$( this).find( "span.no-display" ).css("display","none");});

  //Afficher visuellement le nombre de parts et le nombre de pizzas approprié en dessous du champs « Nombre de parts »
  $("input[type='text']").change(

    function affichage_parts_et_pizzas(){ //fonction qui se charge d'afficher le nombre de pizzas et de parts visuellement
      $("div.nb-parts:last").children().last().removeClass();
      $("span.pizza-6").remove(); //on retire la pizza affichée initialement

      var valeur1 = +$(this).val(); //on récupère la valeur entrée

      var nombre_de_pizzas=Math.floor(valeur1/6) //on récupère la partie entière afin d'afficher les pizzas complètes

      var nombre_de_parts=valeur1%6; //on souhaite obtenir le nombre de parts des pizzas non complètes

      //On divise en deux cas, supérieur à 6 parts et inférieur à 6 parts pour traiter les cas séparément

      if(valeur1>6){

        while(nombre_de_pizzas>0){

          nombre_de_pizzas=nombre_de_pizzas-1;

      $(this).next().before("<span class='pizza-6 pizza-pict'></span>");

      }

        //affichage du nombre de parts des pizzas non complètes
        if(nombre_de_parts==0){$("div.nb-parts:last").children().last().addClass('pizza-0 pizza-pict');}
        else if(nombre_de_parts==1){$("div.nb-parts:last").children().last().addClass('pizza-1 pizza-pict');}
        else if(nombre_de_parts==2){$("div.nb-parts:last").children().last().addClass('pizza-2 pizza-pict');}
        else if(nombre_de_parts==3){$("div.nb-parts:last").children().last().addClass('pizza-3 pizza-pict');}
        else if(nombre_de_parts==4){$("div.nb-parts:last").children().last().addClass('pizza-4 pizza-pict');}
        else if(nombre_de_parts==5){$("div.nb-parts:last").children().last().addClass('pizza-5 pizza-pict');}
        else if(nombre_de_parts==6){$("div.nb-parts:last").children().last().addClass('pizza-6 pizza-pict');}
        else{alert('nombre de parts non possible (entre 1 et 6 seulement)');}

        }

        else if(valeur1<=6){ //on affiche directement le nombre de parts de la pizza demandées si inférieur à 6
          $("div.nb-parts:last").children().last().addClass("pizza-"+ valeur1 +" pizza-pict");
        }

      });

      //Afficher le formulaire de saisie d'adresse au clic sur le bouton "Etape suivante" puis masquer ce même bouton
    	$("button:last").click(function(){
      	$("div.no-display").css("display","block");
      	$(this).remove();
    	});

      //Ajouter une ligne de champ d'adresse lorsque l'on clique sur le bouton "Ajouter un autre ligne d'adresse"
    	$("button.btn-default").click(function(){
    		$(this).before('<br> <input type="text"/>') //ajoute un champ de saisie texte pour le bouton add Ajouter une autre ligne d'adresse
    	});

      $("input[type='text']").change(
      		function gestion_nom(){

          var nom=$("input").eq(-3).val(); //eq(-3) donne l'élément en 3ieme position à partir de la fin, ainsi, cela nous permet de récupérer le nom entré

          // Au clic sur le bouton de validation, supprimer tous les éléments de la page, et afficher un message de remerciement (Merci PRENOM ! Votre commande sera livrée dans 15 minutes).
      	$("button.done").click(function(){ //lorsque l'on clique sur le bouton "Allez hop, j'ai très faim !"
            $("div.row:not(input:eq(-3))").remove();
            $("h1").after("  <br/> <h3> Merci "+ nom + "! Votre commande sera livrée dans 15 minutes </h3> <br/> <br/>");
        	});
          //ATTENTION, bis lorsque adresse entrée également

      	});

      //Actualiser le total de la commande en fonction des éléments choisis grâce à l'attribut data-price. Mettre ce calcul dans une fonction (DRY - Don’t Repeat Yourself)
      //Gestion des prix
      var prix;
      var total;
      var grand_total;

      $("input").change(
        function(){

          total = 0;
          prix=0;
          grand_total=0;


       $('input[name=type]:checked').each( //on ajoute ici le coût selon le type de pizza commandé
         function(){

          prix = +$(this).attr('data-price'); //récupère la valeur d'un attribut
          total = prix + total;

          });


          $('input[name=pate]:checked').each( //on ajoute ici le coût selon le type de pate utilisé
            function() {

          prix = +$(this).attr('data-price')
          total = prix + total;

          });

          $('input[type=checkbox]:checked').each( //on ajoute ici le coût des suppléments
          function() {
            if($(this).prop('checked', true)){

              prix= +$(this).attr('data-price')
              total = prix + total;

            }
            else{prix=0;}

          });

          //ajout somme totale selon nombre de parts
/*
          $('div.nb-parts.input[type=text]').each( //on ajoute ici le coût selon le nombre de parts de pizzas
            function(){

             var prix_parts = +$(this).val()
             grand_total = prix_parts * total;


           });
*/

        $("p:last").text("total par part : " + total + "€ / Grand Total : " + grand_total + "€"); //on affiche le total

      });


}); //fin ready
